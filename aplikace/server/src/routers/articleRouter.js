﻿const express = require('express')
const Article = require('../models/Article')
const {auth, authAdmin} = require('../middleware/auth')

const router = new express.Router()

// Vrátí všechny existující články seřazené dle data vytvoření
 router.get('/articles', auth, async (req, res) => {
    const _id = req.params.id
    try {
        let articles =  await Article.find({})
        .sort([['created', 'descending']])
        .populate('author')
        .populate('comments')
        .lean()
        .exec();

        articles.forEach(i=>{
            let author = i.author;
            author.forEach(j=>{
                delete j.password ;
                delete j.tokens ;
            })
        })

        if (!articles) return res.status(404).send()

        res.send(articles)
    } catch (e) {
        res.status(500).send()
    }
})

// Vrátí existující článek dle id
router.get('/article/:id', auth, async (req, res) => {
    const _id = req.params.id

    try {
        const article = await Article.findById(_id)
        .populate('author')
        .populate('comments')
        .lean()
        .exec();

        if (!article) {
            return res.status(404).send()
        }
        
        // vymazání bezpečnostních otisků z autora
        article.author.forEach(j=>{
            delete j.password ;
            delete j.tokens ;
        })
        
        res.send(article)
    } catch (e) {
        res.status(500).send()
    }
})

// Vytvoří článek
router.post('/article/create', authAdmin, async (req, res) => {
    const article = new Article({
        ...req.body,
        authorId: req.user._id
    })

    try {
        await article.save()
        res.status(201).send(article)
    } catch (e) {
        res.status(400).send(e)
    }
})

// Upraví článek (autor zůstává původní)
router.patch('/article/update/:id', authAdmin, async (req, res) => {
    try {

        let article = await Article.findById(req.params.id)
        
        const updates = Object.keys(req.body)

        updates.forEach((update) => {
            if(update != '_id'){
                article[update] = req.body[update]   
            }         
        })
        article.save()
        res.status(200).send(article)
    } catch (e) {
        res.status(500).send()
    }
})

// Vymaže článek
router.delete('/article/remove/:id', authAdmin, async (req, res) => {
    try {
        const article = await Article.findByIdAndRemove(req.params.id,{ useFindAndModify: false})
        if (!article) {
            return res.status(404).send()
        }
        res.send(article)
    } catch (e) {
        res.status(500).send()
    }
})


module.exports = router
