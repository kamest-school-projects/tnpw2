﻿const express = require('express')
const User = require('../models/User')
const {auth, authAdmin} = require('../middleware/auth')

const router = new express.Router()

// Přihlásí uživatele dle emailu a hesla
router.post('/users/login', async (req, res) => {
    try {
        const user = await User
            .findByCredentials(req.body.email, req.body.password);
        const token = await user.generateAuthToken()
        res.send({ user, token })
    } catch (e) {
        res.status(400).send(e)
    }
})

// Odhlásí uživatele - resp. vymaže jeho přítupový token
router.post('/users/logout', auth, async (req, res) => {
    try {
        req.user.tokens = req.user.tokens.filter((token) => {
            return token.token !== req.token
        })
        await req.user.save()

        res.send()
    } catch (e) {
        res.status(500).send()
    }
})

// Vrátí všechny existující uživatele
router.get('/users',authAdmin, async (req, res) => {
    try {
        const users = await User.find({})
        res.send(users)
    } catch(e) {
        res.status(500).send()
    }
})

// Vytvoří uživatele
router.post('/users/create', async (req, res) => {
    const user = new User(req.body)
    try {
        await user.save()
        const token = await user.generateAuthToken()
        res.status(201).send({ user, token })
    } catch (e) {
        res.status(400).send(e)
    }
})

// Upraví uživatele - pouze hodnoty : 'name', 'admin', 'email', 'mobile'
router.patch('/users/update/:id', authAdmin, async (req, res) => {
    const updates = Object.keys(req.body)
    const allowedUpdates = ['name', 'admin', 'email', 'mobile']

    try {
        let user = await User.findById(req.params.id)
        updates.forEach((update) => {
            if(allowedUpdates.includes(update)){
                user[update] = req.body[update]
            }
        })
        User.updateOne(user);
        user.save()
        if (!user) {
            return res.status(404).send()
        }

        res.send(user)
    } catch (e) {
        res.status(400).send(e)
    }
})

// Vymaže uživatele dle id
router.delete('/users/delete/:id', authAdmin, async (req, res) => {
    try {
        const user = await User.findByIdAndDelete(req.params.id)

        if (!user) {
            return res.status(404).send()
        }
        res.status(200).send(user)
    } catch (e) {
        res.status(500).send()
    }
})

module.exports = router
