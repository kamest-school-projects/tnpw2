const mongoose = require('mongoose')
const validator = require('validator')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const strength = require("strength");
const getProperty = require('../util/util')

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        trim: true
    },
    email: {
        type: String,
        required: true,
        unique: true,
        trim: true,
        lowercase: true,
        validate(value) {
            if (!validator.isEmail(value)) {
                throw new Error('Neplatná mailová adresa!')
            }
        }
    },
    password: {
        type: String,
        required: true,
        minlength: 7,
        trim: true,
        validate(value) {
            let howStrong = strength(value);
            if (howStrong<2) {
                throw new Error('Heslo musí být silnější!')
            }
        }
    },
    mobile: {
        type: String,
        required: true,
        minlength: 9,
        trim: true,
        validate(value) {
            if (!validator.isMobilePhone(value,'cs-CZ')) {
                throw new Error('Telefoní číslo není platné!')
            }
        }
    },
    admin: {
        type: Boolean
    },
    tokens: [{
        token: {
            type: String,
            required: true
        }
    }]
})

// Generuje token pro komunikaci přihlášeného uživatele client-server
// následně se zasílá v hlavičkách
userSchema.methods.generateAuthToken = async function () {
    const user = this
    const token = jwt.sign({ _id: user._id.toString() }, getProperty('auth.token'))

    user.tokens = user.tokens.concat({ token })
    await user.save()

    return token
}

// Odstraní z jsonu password a tokeny kvůli bezpečnosti
// POZOR - entity, které mají User entitu jako populated 
// nepoužívají tuto metodu a musí se odmazat ručně
userSchema.methods.toJSON = function () {
    const user = this
    const userObject = user.toObject()
    delete userObject.password
    delete userObject.tokens
    return userObject
}

// Najde uživatele dle přihlašovacích údajů
userSchema.statics.findByCredentials = async (email, password) => {
    const user = await User.findOne({'email':  email })

    if (!user) {
        throw new Error('Přihlášení selhalo - uživatel nenalezen')
    }

    const isMatch = await bcrypt.compare(password, user.password)

    if (!isMatch) {
        throw new Error('Přihlášení selhalo - údaje neodpovídají')
    }

    return user
}

// Zakriptuje heslo před uložením
userSchema.pre('save', async function (next) {
    const user = this

    if (user.isModified('password')) {
        user.password = await bcrypt.hash(user.password, 8)
    }

    next()
})

const User = mongoose.model('User', userSchema)

module.exports = User
