const mongoose = require('mongoose')
const Comment = require('./Comment')

const articleSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
        trim: true
    },
    motive: {
        data: Buffer, 
        contentType: String
    },
    content: {
        type: String,
        required: true,
        trim: true
    },
    created: {
        type: Date
    },
    updated: {
        type: Date
    },
    authorId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'User'
    }
})

// Přidá do objektu modelu objekt autora (pomocí populate('author'))
articleSchema.virtual('author', {
    ref: 'User',
    localField: 'authorId',
    foreignField: '_id'
})

// Přidá do objektu modelu objekty komentářů (pomocí populate('comments'))
articleSchema.virtual('comments', {
    ref: 'User',
    localField: '_id',
    foreignField: 'articleId'
})

// Přidá datum vytvoření / změny
articleSchema.pre('save', async function (next) {
    if (!this.created) {
        this.created = Date.now();
    }else {
        this.updated= Date.now();
    }

    next()
})

// Při smazání příspěvku se vymaží i navázané komentáře
articleSchema.pre('remove', async function (next) {
    await Comment.deleteMany({ articleId: this._id })
    next()
})

const Article = mongoose.model('Article', articleSchema)
module.exports = Article
