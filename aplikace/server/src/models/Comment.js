const mongoose = require('mongoose')
const User = require('./User')

const commentSchema = new mongoose.Schema({
    content: {
        type: String,
        required: true,
        trim: true
    },
    created: {
        type: Date
    },
    updated: {
        type: Date
    },
    authorId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'User'
    },
    articleId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'Article'
    }
})

// Přidá do objektu modelu objekt autora (pomocí populate('commentAuthor'))
commentSchema.virtual('commentAuthor', {
    ref: 'User',
    localField: 'authorId',
    foreignField: '_id'
})

// Přidá datum vytvoření / změny
commentSchema.pre('save', async function (next) {
    if (!this.created) {
        this.created = Date.now();
    }else {
        this.updated= Date.now();
    }

    next()
})

const Comment = mongoose.model('Comment', commentSchema)
module.exports = Comment
