const PropertiesReader = require('properties-reader');
const prop = PropertiesReader('app.properties');

// Vrátí hodntou uloženou v app.properties
const getProperty = (pty) => {
    return prop.get(pty);
};

module.exports = getProperty
