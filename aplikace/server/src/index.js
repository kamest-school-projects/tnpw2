﻿const express = require('express')
const path = require('path')
const getProperty = require('./util/util')
require('./db/mongoose')

const userRouter = require('./routers/userRouter')
const commentRouter = require('./routers/commentRouter')
const articleRouter = require('./routers/articleRouter')

const app = express()
const port = process.env.PORT || 3100


app.use(express.json())

//Pro cors ochranu na odeslání z reactu, na prostření je nutné zařadit správnou tld do properties
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", getProperty("cors.allowTld"));
  res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT,DELETE,PATCH");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization,access-control-allow-credentials");
  next();
});
  
  
//Registrace jednotlivých routerů
app.use(articleRouter)
app.use(commentRouter)
app.use(userRouter)

app.listen(port, () => {
    console.log('Server poslouchá na portu ' + port)
})
