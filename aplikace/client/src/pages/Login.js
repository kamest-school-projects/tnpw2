import React, { useState } from "react";
import { Redirect, Link } from "react-router-dom";
import axios from 'axios';
import { Button, Error } from "../components/AuthForms";

function Login(props) {
  const [isLoggedIn, setLoggedIn] = useState(false);
  const [isError, setIsError] = useState(false);
  const [userName, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const referer = props.location.state  && props.location.state.referer ?  props.location.state.referer : '/home';

  function postLogin(e) {
    e.preventDefault();
    // Vygenerování tokenů na serveru
    axios.post("http://localhost:3100/users/login", {
      'email' : userName,
      'password' : password,
    }).then(result => {
      if (result.status === 200) {
        // Uložení do lokální storage
        localStorage.setItem('user', JSON.stringify(result.data))
        setLoggedIn(true);
        window.location.reload(false);
      } else {
        setIsError(true);
      }
    }).catch(e => {
      setIsError(true);
      console.log('result ' + JSON.stringify(e))
    });
  }

  if (isLoggedIn) {
    return <Redirect to={referer} />;
  }

  return (
    <main>
      <center>
        <div className="section"></div>

        <h5 className="indigo-text">Prosím, přihlašte se..</h5>
        <div className="section"></div>

        <div className="container">
          <div className="z-depth-1 grey lighten-4 row login" >

            <form className="col s12">
              <div className='row'>
                <div className='col s12'>
                </div>
              </div>

              <div className='row'>
                <div className='input-field col s12'>
                  <input 
                      className='validate' 
                      type='email' 
                      name='email' 
                      id='email'
                      placeholder="email"
                      value={userName}
                      onChange={e => {
                        setUserName(e.target.value);
                      }} />
                </div>
              </div>

              <div className='row'>
                <div className='input-field col s12'>
                  <input 
                    className='validate' 
                    type='password' 
                    name='password' 
                    id='password'
                    placeholder="heslo"
                    value={password}
                    onChange={e => {
                      setPassword(e.target.value);
                    }} />
                </div>
              </div>

              <br />
              <center>
                <div className='row'>
                 <Button type='submit' name='btn_login' className='col s12 btn btn-large waves-effect indigo' onClick={postLogin}>Přihlásit</Button>

                 { isError &&<Error>Stala se nějaká chyba,zkuste to znovu,nebo se prosím ozvěte administrátorům !</Error> }
                </div>
              </center>
            </form>
          </div>
        </div>
        <Link to="/signup" className="indigo-text">Potřebujete založit účet?</Link>
      </center>

      <div className="section"></div>
      <div className="section"></div>
    </main>
  );
}

export default Login;