import React from "react";
import ReactDOM from 'react-dom';
import { Link } from "react-router-dom";
import axios from 'axios';
import ArticleModal from '../components/ArticleModal';

/**
 * Slouží pouze pro výpis článků
 */
class Home extends React.Component {
  state = {
    articles: []
  }

  fetchData() {
    const user = JSON.parse(localStorage.getItem('user'));
    const config = {
      headers: { 
        'Authorization': `Bearer ${user.token}`}
    };

    axios.get("http://localhost:3100/articles/",config)
    .then(result => {
      if (result.status === 200) {
        this.setState({
          articles: result.data
        })
      } else {
        console.log('result ' + result.status)
      }
    }).catch(e => {
      console.log('result ' + JSON.stringify(e))
    });
  }

  componentDidMount() {
    this.fetchData();
  }

  render(){
    const { articles } = this.state;

    let user = null;
    const item = localStorage.getItem('user');
    if(item !== null){
      const optUser = JSON.parse(item);
      user=optUser;
    }
    const isAdmin = user.user.admin;
    const { name } = user.user;
    
    //Výpis článků
    return (
      <div className="section no-pad-bot" id="index-banner">
        <div className="container">
          { isAdmin ? (
            <div className="row">
              <div className="col s9">
                <h3>Ahoj, {name}</h3>
              </div>
              <div className="col s3 right-align">
                <h3>
                  <a href="/#/"
                    className="btn-floating  btn-large waves-effect waves-light red"
                    onClick={(e)=>{
                      e.preventDefault()
                      ReactDOM.render(
                        <ArticleModal />, 
                        document.getElementById("articleModalHolder")
                      );
                    }}>
                      <i className="material-icons">add</i>
                  </a>
                </h3>

              </div>
            </div>
            ): <div/>}

            <div className="row">
              {
                articles.map(article => {
                  const { _id, title, updated, created } = article;
                  return (
                    <div key={_id} id={_id} className="col s12 m6 l3">
                      <div className="card">
                        <div className="card-content">
                          <span className="card-title activator grey-text text-darken-4">
                            {title}
                          </span>
                          <p><Link to={"/article/" + _id}>Více</Link></p>
                          <p>{new Date(created).toLocaleDateString()}</p>
                          {updated ? <p>{new Date(updated).toLocaleDateString()}</p>: ""}
                        </div>
                      </div>
                    </div>
                  );
                })
              }
            </div>
        </div>
        {/* util pro vložení ArticleModal */}
        <div id="articleModalHolder"></div>
      </div>
    )}
}

export default Home;