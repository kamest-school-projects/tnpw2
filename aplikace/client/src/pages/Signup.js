import React, {useState} from "react";
import { Redirect } from "react-router-dom";
import axios from 'axios';
import {  Error } from "../components/AuthForms";
function Signup(props) {
  const [userName, setUserName] = useState("");
  const [isLoggedIn, setLoggedIn] = useState(false);
  const [password, setPassword] = useState("");
  const [admin, setAdmin] = useState(false);
  const [isError, setIsError] = useState(false);
  const [name, setName] = useState("");
  const [mobile, setMobile] = useState("");
  const referer = props.location.state  && props.location.state.referer ?  props.location.state.referer : '/home';

  function register(e) {
    e.preventDefault();
    // Vyvoření uživatele a vygenerování tokenu
    axios.post("http://localhost:3100/users/create", {
      'email' : userName,
      'password' : password,
      'name' : name,
      'mobile' : mobile,
      'admin':admin
    }).then(result => {
      if (result.status === 201) {
        // uložení tokenu
        localStorage.setItem('user', JSON.stringify(result.data))
        setLoggedIn(true);
        window.location.reload(false);
      } else {
        setIsError(true);
      }
    }).catch(e => {
      setIsError(true);
      console.log('result ' + JSON.stringify(e))
    });
  }

  if (isLoggedIn) {
    return <Redirect to={referer} />;
  }
  return (

    <main>
      <center>
        <div className="section"></div>

        <h5 className="indigo-text">Register to see more...</h5>
        <div className="section"></div>

        <div className="container">
          <div className="z-depth-1 grey lighten-4 row signUp">

            <form className="col s12" onSubmit={register}>
              <div className='row'>
                <div className='input-field col s12'>
                  <input 
                    className='validate' 
                    type='text' 
                    name='name' 
                    id='name' 
                    placeholder="Jméno"
                    value={name}
                    onChange={e => {
                      setName(e.target.value);
                    }} />
                </div>
              </div>

              <div className='row'>
                <div className='input-field col s12'>
                  <input 
                    className='validate'
                    type='email' 
                    name='email' 
                    id='email'
                    placeholder="Email"
                    value={userName}
                    onChange={e => {
                      setUserName(e.target.value);
                    }} />
                </div>
              </div>

              <div className='row'>
                <div className='input-field col s12'>
                  <input 
                    className='validate' 
                    type='number' 
                    name='mobile' 
                    id='mobile'
                    placeholder="Mobil"
                    value={mobile}
                    onChange={e => {
                      setMobile(e.target.value);
                    }} />
                </div>
              </div>
              <div className='row'>
                <div className='input-field col s12'>
                  <input 
                    className='validate' 
                    type='password' 
                    name='password' 
                    id='password'
                    placeholder="Heslo"
                    value={password}
                    onChange={e => {
                      setPassword(e.target.value);
                    }} />
                </div>
              </div>

              <div className='row'>
                <div className='input-field col s12'>
                  <label>
                    <input 
                      type="checkbox" 
                      className="filled-in"
                      value={admin}
                      checked={admin}
                      onClick={e => {
                          setAdmin(!admin)
                      }}/>       
                    <span>Admin <strong>(pouze pro testovací účely)</strong></span>
                  </label>
                </div>
              </div>

              <br />
              <center>
                <div className='row'>
                  <button className='col s12 btn btn-large waves-effect indigo'>Registrovat</button>
                  { isError &&<Error>Stala se nějaká chyba,zkuste to znovu,nebo se prosím ozvěte administrátorům !</Error> }
                </div>
              </center>
            </form>
          </div>
        </div>
      </center>
    </main>
  );
}

export default Signup;

