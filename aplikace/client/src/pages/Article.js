import React from "react";
import ReactDOM from 'react-dom';
import axios from 'axios';
import CommentModal from '../components/CommentModal';
import ArticleModal from '../components/ArticleModal';

const headStyle = {
  padding: '2em',
};
const largerText = {
  fontSize: '1.2em',
};
const twoPaddingSides = {
  padding: '2em 0',
};
const vAli = {
  verticalAlign: 'super',
  paddingLeft: '0.5em',
};


class Article extends React.Component {

  state = {
    article: [],
    comments: []
  }

  componentDidMount() {
    const user = JSON.parse(localStorage.getItem('user'));
    const config = {
      headers: { 
        'Authorization': `Bearer ${user.token}`}
    };

    axios.get("http://localhost:3100/article/"+this.props.match.params.id, config)
    .then(result => {
      if (result.status === 200) {
        this.setState({
          article: result.data
        })
      } else {
        console.log('result ' + result.status)
      }
    }).catch(e => {
      console.log('result ' + JSON.stringify(e))
    });

    axios.get("http://localhost:3100/comments/"+this.props.match.params.id, config)
    .then(result => {
      if (result.status === 200) {
        this.setState({
          comments: result.data
        })
      } else {
        console.log('result ' + result.status)
      }
    }).catch(e => {
      console.log('result ' + JSON.stringify(e))
    });
  }


  removeComment(e) {
    e.preventDefault()
    const user = JSON.parse(localStorage.getItem('user'));
    const config = {
      headers: { 
        'Authorization': `Bearer ${user.token}`}
    };
    let nodeId = e.currentTarget.id;
    let id = nodeId.replace("delete_com_","")
    // odstranění článku
    axios.delete("http://localhost:3100/comment/remove/"+id, config)
    .then(result => {
      if (result.status === 200) {
      } else {
        console.log('result ' + result.status)
      }
    }).catch(e => {
      console.log('result ' + JSON.stringify(e))
    });
  }

  render(){

    const { article,comments } = this.state;
    const user = JSON.parse(localStorage.getItem('user'));
    const isAdmin = user.user.admin;
    const { _id, content, title, authorId,author } = article;
    let authorName = undefined;
    let authorEmail = undefined;
    let authorMobile = undefined;
    if(author){
      const articleAuthor = author.pop();
      author.push(articleAuthor);
      if(articleAuthor){
        authorName = articleAuthor.name;
        authorEmail = articleAuthor.email;
        authorMobile = articleAuthor.mobile;
      }
    }
    return (

      <div>
        {/* Hlavní obsah */}
        <div className="section no-pad-bot" id="index-banner">
          <div className="container">
            <div className="row">
              <div className="col s9">
                <h3>{title}</h3>
              </div>
              <div className="col s3 right-align">
                {
                  isAdmin || authorId === user.user._id?
                  <h3>
                    <a href="/#/" className="btn-floating  btn-large waves-effect waves-light red modal-trigger" 
                    onClick={(e)=>{
                      e.preventDefault()
                      ReactDOM.render(
                        <ArticleModal 
                          content={article.content}
                          title={article.title}
                          motive={article.motive}
                          aId={article._id}
                          />, 
                        document.getElementById("articleModalHolder")
                      );
                    }}>
                      <i className="material-icons">edit</i>
                    </a>
                  </h3> : ""
                }

              </div>
            </div>
            <div className="row">

                <div className="col s9">
                  <div style={headStyle}>
                      {content}
                    </div>      
                  </div>
                  <div className="col s3" style={largerText}>
                    <h5>{authorName}</h5>
                    <p>{authorEmail}</p>
                    <p>{authorMobile}</p>
                  </div>  
                
            </div>
          </div>
          {/* Komentáře */}
          <div className="container">
            <div className="row" style={twoPaddingSides}>

              <div className="col s9 right-align">
                <a href="/#/" 
                  className="btn-floating  btn-large waves-effect waves-light red modal-trigger" 
                  onClick={(e)=>{
                      e.preventDefault()
                      ReactDOM.render(
                        <CommentModal
                          articleId={_id}/>, 
                        document.getElementById("commentModalHolder")
                      );
                    }}>
                  <i className="material-icons">add</i>
                  </a>

              </div>
              <div className="col s9">
                {
                  comments.map(comment => {
                    let commentAuthorObject = comment.commentAuthor.pop();
                    comment.commentAuthor.push(commentAuthorObject);
                    let commentAuthorName = commentAuthorObject.name;
                    return(

                      <div  key={_id} className="card blue">
                        <div className="card-content white-text">
                          <span className="card-title rounded">
                            <span style={vAli}> {commentAuthorName}</span>
                          </span>
                          <p>{comment.content}</p>
                          <p><small>{new Date(comment.created).toLocaleDateString()}</small></p>
                          {comment.updated ? <p><small>{new Date(comment.updated).toLocaleDateString()}</small></p>: ""}
                        </div>
                        
                          {
                            isAdmin || comment.authorId === user.user._id?
                            <div className="card-action">
                              <a href="/#/" 
                                className="waves-effect waves-light modal-trigger"
                                id={"edit_com_"+comment._id}
                                onClick={(e)=>{
                                    e.preventDefault()
                                    ReactDOM.render(<CommentModal commentId={comment._id} reload={this.componentDidMount} content={comment.content}  articleId={_id}/>, document.getElementById("commentModalHolder"));
                                }}
                                >
                                  <i className="material-icons red-text text-lighten-3">
                                    edit
                                  </i>
                              </a>
                              <a href="/#/" className="waves-effect waves-light" onClick={this.removeComment} id={"delete_com_"+comment._id}><i className="material-icons red-text text-lighten-3">delete</i></a> 
                              </div> : ""}
                          
                      </div>
                    )
                  })
                }
                  
              </div>
            </div>
          </div>
        {/* util pro vložení ArticleModal a CommentModal */}
        <div id="articleModalHolder"></div>
        <div id="commentModalHolder"></div>
          
      </div>
      </div>

    );
  }
}

export default Article;