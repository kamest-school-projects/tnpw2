import React, { useState } from "react";
import axios from 'axios';
import { Error } from "./AuthForms";

const modalStyle={
  zIndex: "1003", 
  display: "block", 
  opacity: "1", 
  bottom: "0px",
}
const modalOverlay = {
  zIndex: "1002",
  display: "block",
  opacity: "0.5",
}

/**
 * Slouží pro editaci článků
 */
function ArticleModal(props) {

  const [title, setTitle] = useState(props.title);
  const [content, setContent] = useState(props.content);
  const [isError, setIsError] = useState(false);

  
  function sendComment(e){
    e.preventDefault()
    const user = JSON.parse(localStorage.getItem('user'));
    const config = {
      headers: { 
        'Authorization': `Bearer ${user.token}`}
    };
    let articleId = props.aId;
    if(articleId == null){
      // odeslání článku jako nového
      axios.post("http://localhost:3100/article/create", {
        'content' : content,
        'title':title
      },config).then(result => {
        if (result.status === 201) {
          window.location.reload(false);
        }else{
          setIsError(true);
        }
      }).catch(e => {
        setIsError(true);
        console.log('result ' + JSON.stringify(e))
      });
    }else{
      // editace článku
      axios.patch("http://localhost:3100/article/update/"+articleId, {
        'content' : content,
        'title':title
      },config).then(result => {
        if (result.status === 200 ) {
          window.location.reload(false);
        }else{
          setIsError(true);
        }
      }).catch(e => {
        setIsError(true);
        console.log('result ' + JSON.stringify(e))
      });
    }
  }

  return (
    <div>
      <div id="modalTargetComment" className="modal bottom-sheet" style={modalStyle}>
        <div className="modal-content">
          <h5>{props.aId? "Úprava" : "Vytvoření"} článku</h5>
          <div className="row">
            <form className="col s12" encType="multipart/form-data">
              <div className="row">
                  <div className="input-field col s6">
                    <input 
                      id="title" 
                      type="text" 
                      className="validate"
                      value={title}
                      onChange={e => {
                        setTitle(e.target.value);
                      }} />
                    <label htmlFor="title">Název článku</label>
                  </div>
                <div className="input-field col s12">
                  <textarea 
                    id="textarea1" 
                    className="materialize-textarea"
                    value={content}
                    onChange={e => {
                      setContent(e.target.value);
                    }} ></textarea>
                  <label htmlFor="textarea1">Obsah</label>
                </div>
                <div className="input-field col s12">
                  <button className="btn waves-effect waves-light indigo" type="submit" name="action" onClick={sendComment}>Odeslat
                    <i className="material-icons right">send</i>
                  </button>
                    { isError &&<Error>Stala se nějaká chyba,zkuste to znovu,nebo se prosím ozvěte administrátorům !</Error> }
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div className="modal-overlay" style={modalOverlay}></div>
    </div>
  );
}

export default ArticleModal;