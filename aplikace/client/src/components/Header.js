// import React from "react";
import { Link } from "react-router-dom";
import axios from 'axios';
import React from "react";

function Header(props) {

  function logOut() {

    const user = JSON.parse(localStorage.getItem('user'));
    const config = {
      headers: { 
        'Authorization': `Bearer ${user.token}`}
    };
    // Vymazání tokenu na serveru
    axios.post("http://localhost:3100/logout/", user,config)
    .then(result => {
      if (result.status === 200) {
        window.location.reload(false);
      } else {
        console.log('result ' + result.status)
      }
    }).catch(e => {
      console.log('result ' + JSON.stringify(e))
    });
    
    // Vymazání tokenu a objektu uživatele v lokální storage
    localStorage.setItem('user',null)
  }

  let user = null;
  const item = localStorage.getItem('user');
  if(item !== null){
    const optUser = JSON.parse(item);
    if(optUser != null && optUser.token) user=optUser;
  }

  return (
    <nav className="indigo lighten-1" role="navigation">
      <div className="nav-wrapper container"><Link id="logo-container"  to="/home" className="brand-logo">Notitia</Link>
        <ul className="right hide-on-med-and-down">
          <li><Link to="/home">Home</Link></li>
          {user && user.user.admin ? (
              <span>
                <li><Link to="/admin">Admin</Link></li>
                <li><Link to="/home" onClick={logOut}>Logout</Link></li>
              </span>
            ) : (
                user ? <li><Link to="/home" onClick={logOut}>Logout</Link></li> :
              <li><Link to={{ pathname: "/login", state: { referer: props.location } }}>Login</Link></li>
            )}
        </ul>

        {/* pro mobilí telefony */}
        <ul id="nav-mobile" className="sidenav">
          <li><Link to="/home">Home</Link></li>
          {user && user.admin ? (
              <span>
                <li><Link to="/admin">Admin</Link></li>
                <li><Link to="/home" onClick={logOut}>Logout</Link></li>
              </span>
            ) : (
                user ? <li><Link to="/home" onClick={logOut}>Logout</Link></li> :
              <li><Link to={{ pathname: "/login", state: { referer: props.location } }}>Login</Link></li>
            )}
        </ul>
        <a href="/#/" data-target="nav-mobile" className="sidenav-trigger"><i className="material-icons">menu</i></a>
      </div>
    </nav>
  );
}

export default Header;