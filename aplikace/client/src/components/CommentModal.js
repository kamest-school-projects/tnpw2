import React, { useState } from "react";
import axios from 'axios';
import { Error } from "../components/AuthForms";

const modalStyle={
  zIndex: "1003", 
  display: "block", 
  opacity: "1", 
  bottom: "0px",
}
const modalOverlay = {
  zIndex: "1002",
  display: "block",
  opacity: "0.5",
}

/**
 * Slouží pro editaci komentářů v popupu
 */
function CommentModal(props) {

  const [content, setContent] = useState(props.content);
  const [isError, setIsError] = useState(false);

  
  function sendComment(e){
    e.preventDefault()
    const user = JSON.parse(localStorage.getItem('user'));
    const config = {
      headers: { 
        'Authorization': `Bearer ${user.token}`}
    };
    let commentId = props.commentId;
    if(commentId == null){
      // Vytvoření
      axios.post("http://localhost:3100/comment/create", {
        'content' : content,
        'articleId' : props.articleId,
      },config).then(result => {
        if (result.status === 201) {
          window.location.reload(false);
        }else{
          setIsError(true);
        }
      }).catch(e => {
        setIsError(true);
        console.log('result ' + JSON.stringify(e))
      });
    }else{
      // Editace
      axios.patch("http://localhost:3100/comment/update/"+commentId, {
        'content' : content,
        'articleId' : props.articleId,
      },config).then(result => {
        if (result.status === 200) {
          window.location.reload(false);
          return;
        }else{
          setIsError(true);
        }
      }).catch(e => {
        setIsError(true);
        console.log('result ' + JSON.stringify(e))
      });
    }
  }

  return (
    <div>
      <div id="modalTargetComment" className="modal bottom-sheet" style={modalStyle}>
        <div className="modal-content">
          <h5>{props.commentId? "Úprava" : "Vytvoření"} komentáře</h5>
          <div className="row">
            <form className="col s12" >
              <div className="row">
                <div className="input-field col s12">
                  <textarea 
                    id="textarea1" 
                    className="materialize-textarea"
                    value={content}
                    onChange={e => {
                      setContent(e.target.value);
                    }} ></textarea>
                  <label htmlFor="textarea1">Text komentáře</label>
                </div>
                <div className="input-field col s12">
                  <button className="btn waves-effect waves-light indigo" type="submit" name="action" onClick={sendComment}>Odeslat
                    <i className="material-icons right">send</i>
                  </button>
                    { isError &&<Error>Stala se nějaká chyba,zkuste to znovu,nebo se prosím ozvěte administrátorům !</Error> }
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="modal-overlay" style={modalOverlay}></div>
    </div>
  );
}

export default CommentModal;